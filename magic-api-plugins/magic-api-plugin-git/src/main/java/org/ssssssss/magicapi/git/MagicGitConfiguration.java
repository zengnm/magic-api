package org.ssssssss.magicapi.git;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.ssssssss.magicapi.core.config.MagicAPIProperties;
import org.ssssssss.magicapi.core.config.MagicPluginConfiguration;
import org.ssssssss.magicapi.core.config.Resource;
import org.ssssssss.magicapi.core.model.MagicEntity;
import org.ssssssss.magicapi.core.model.Plugin;
import org.ssssssss.magicapi.core.service.MagicResourceService;
import org.ssssssss.magicapi.core.service.MagicResourceStorage;
import org.ssssssss.magicapi.core.service.impl.ApiInfoMagicResourceStorage;
import org.ssssssss.magicapi.function.model.FunctionInfo;
import org.ssssssss.magicapi.git.registry.MultiBranchFunctionMagicDynamicRegistry;
import org.ssssssss.magicapi.git.registry.MultiBranchRequestMagicDynamicRegistry;
import org.ssssssss.magicapi.utils.Mapping;

import java.io.IOException;
import java.util.List;

@Configuration
@EnableConfigurationProperties(MagicGitProperties.class)
public class MagicGitConfiguration implements MagicPluginConfiguration {

	private final MagicAPIProperties properties;
	private final MagicGitProperties gitProperties;

	public MagicGitConfiguration(MagicAPIProperties properties, MagicGitProperties gitProperties) {
		this.properties = properties;
		this.gitProperties = gitProperties;
	}

	/**
	 * git存储
	 * @author soriee
	 * @date 2022/2/28 19:50
	 * @return
	 */
	@Bean
	@ConditionalOnMissingBean
	@ConditionalOnProperty(prefix = "magic-api", name = "resource.type", havingValue = "git")
	public org.ssssssss.magicapi.core.resource.Resource magicGitResource() throws IOException, GitAPIException {
		Resource resourceConfig = properties.getResource();
		return GitResource.of(resourceConfig, this.gitProperties);
	}

	@Bean
	@ConditionalOnMissingBean
	@ConditionalOnProperty(prefix = "magic-api", name = "resource.type", havingValue = "git")
	public MagicResourceService magicResourceService(ObjectProvider<List<MagicResourceStorage<? extends MagicEntity>>> magicResourceStoragesProvider,
													 org.ssssssss.magicapi.core.resource.Resource magicGitResource,
													 ApplicationContext applicationContext) {
		return new MultiBranchMagicResourceService(magicResourceStoragesProvider.getObject(), applicationContext,
				magicGitResource, this.gitProperties);
	}

	@Bean
	@ConditionalOnProperty(prefix = "magic-api", name = "resource.type", havingValue = "git")
	public MultiBranchFunctionMagicDynamicRegistry functionMagicDynamicRegistry(MagicResourceStorage<FunctionInfo> magicResourceStorage,
																				MagicGitProperties gitProperties) {
		return new MultiBranchFunctionMagicDynamicRegistry(magicResourceStorage, gitProperties);
	}

	@Bean
	@ConditionalOnProperty(prefix = "magic-api", name = "resource.type", havingValue = "git")
	public MultiBranchRequestMagicDynamicRegistry magicRequestMagicDynamicRegistry(ApiInfoMagicResourceStorage apiInfoMagicResourceStorage, RequestMappingHandlerMapping requestMappingHandlerMapping, MagicAPIProperties properties, MagicGitProperties gitProperties) throws NoSuchMethodException {
		return new MultiBranchRequestMagicDynamicRegistry(apiInfoMagicResourceStorage,
				Mapping.create(requestMappingHandlerMapping, properties.getWeb()), properties.isAllowOverride(),
				properties.getPrefix(), gitProperties);
	}

	@Override
	public Plugin plugin() {
		return new Plugin("Git");
	}
}
