package org.ssssssss.magicapi.git;

import org.ssssssss.magicapi.core.resource.FileResource;
import org.ssssssss.magicapi.core.resource.Resource;
import org.ssssssss.magicapi.utils.IoUtils;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 指定git分支读写
 *
 * @author zengnianmei
 */
public class GitBranchResource extends FileResource {
    private final Resource delegate;
    private final GitRepo gitRepo;
    private final String branch;

    private GitBranchResource(File file, Resource delegate, GitRepo gitRepo, String branch) {
        super(file, delegate.readonly(), gitRepo.getRootPath());
        this.delegate = delegate;
        this.gitRepo = gitRepo;
        this.branch = branch;
    }

    public static GitBranchResource of(Resource delegate, GitRepo gitRepo, String branch) {
        File file = new File(gitRepo.getRootPath());
        return new GitBranchResource(file, delegate, gitRepo, branch);
    }

    private GitBranchResource newResource(File file) {
        GitResource gitResource = new GitResource(this.readonly(), file, gitRepo.getRootPath(), gitRepo);
        return new GitBranchResource(file, gitResource, this.gitRepo, this.branch);
    }

    private <T> T checkoutAndDo(Supplier<T> writeAction) {
        // 加锁，任何一个操作必须保证在对应分支上进行
        synchronized (gitRepo) {
            gitRepo.checkout(this.branch);
            return writeAction.get();
        }
    }

    @Override
    public boolean exists() {
        return checkoutAndDo(super::exists);
    }

    @Override
    public boolean delete() {
        return checkoutAndDo(delegate::delete);
    }

    @Override
    public boolean isDirectory() {
        return checkoutAndDo(super::isDirectory);
    }

    @Override
    public byte[] read() {
        return checkoutAndDo(() -> IoUtils.bytes(this.file));
    }

    @Override
    public boolean mkdir() {
        return checkoutAndDo(delegate::mkdir);
    }

    @Override
    public Resource getResource(String name) {
        return checkoutAndDo(() -> newResource(new File(super.file, name)));
    }

    @Override
    public Resource getDirectory(String name) {
        return getResource(name);
    }

    @Override
    public boolean write(byte[] bytes) {
        return checkoutAndDo(() -> delegate.write(bytes));
    }

    @Override
    public boolean write(String content) {
        return checkoutAndDo(() -> delegate.write(content));
    }

    @Override
    public List<Resource> resources() {
        return checkoutAndDo(() -> {
            File[] files = this.file.listFiles();
            return files == null ? Collections.emptyList() :
                    Arrays.stream(files).map(this::newResource).collect(Collectors.toList());
        });
    }

    @Override
    public Resource parent() {
        return checkoutAndDo(() -> this.rootPath.equals(this.file.getAbsolutePath()) ? null : newResource(this.file.getParentFile()));
    }

    @Override
    public List<Resource> dirs() {
        return checkoutAndDo(() -> IoUtils.dirs(this.file).stream().map(this::newResource)
                .collect(Collectors.toList()));
    }

    @Override
    public List<Resource> files(String suffix) {
        return checkoutAndDo(() -> IoUtils.files(this.file, suffix).stream().map(this::newResource)
                .collect(Collectors.toList()));
    }

    @Override
    public boolean renameTo(Resource resource) {
        return checkoutAndDo(() -> delegate.renameTo(resource));
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
