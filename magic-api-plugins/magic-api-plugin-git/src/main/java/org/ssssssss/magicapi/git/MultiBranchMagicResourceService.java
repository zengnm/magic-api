package org.ssssssss.magicapi.git;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.ssssssss.magicapi.core.config.JsonCodeConstants;
import org.ssssssss.magicapi.core.model.Group;
import org.ssssssss.magicapi.core.model.MagicEntity;
import org.ssssssss.magicapi.core.model.MagicNotify;
import org.ssssssss.magicapi.core.model.SelectedResource;
import org.ssssssss.magicapi.core.model.TreeNode;
import org.ssssssss.magicapi.core.resource.Resource;
import org.ssssssss.magicapi.core.service.MagicResourceService;
import org.ssssssss.magicapi.core.service.MagicResourceStorage;
import org.ssssssss.magicapi.core.service.impl.DefaultMagicResourceService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zengnianmei
 */
public class MultiBranchMagicResourceService implements MagicResourceService, JsonCodeConstants, ApplicationListener<ApplicationStartedEvent> {
    private final List<MagicResourceStorage<? extends MagicEntity>> storages;
    private final ApplicationEventPublisher publisher;
    private final MagicGitProperties properties;
    private final Resource resource;
    private final GitRepo gitRepo;

    private final Map<String, DefaultMagicResourceService> delegateMap = new ConcurrentHashMap<>();


    public MultiBranchMagicResourceService(List<MagicResourceStorage<? extends MagicEntity>> storages,
                                           ApplicationEventPublisher publisher,
                                           Resource resource,
                                           MagicGitProperties properties) {
        this.storages = storages;
        this.publisher = publisher;
        this.resource = resource;
        this.properties = properties;
        this.gitRepo = ((GitResource)resource).getGitRepo();
    }

    private void resetDelegateMap(boolean triggerEvent) {
        delegateMap.clear();
        for (String branch : gitRepo.getAllBranch()) {
            GitBranchResource multiBranchResource = GitBranchResource.of(resource, gitRepo, branch);
            DefaultMagicResourceService service = new DefaultMagicResourceService(multiBranchResource, this.storages,
                    this.publisher);
            GitUtils.setCurrentBranch(branch);
            if (triggerEvent) {
                service.refresh();
            } else {
                service.onApplicationEvent(null);
            }
            delegateMap.put(branch, service);
        }
    }

    private DefaultMagicResourceService getDelegate() {
        String baseBranch = this.properties.getBranch();
        String branchHeader = this.properties.getBranchHeader();
        String currentBranch = GitUtils.getCurrentBranch(branchHeader, baseBranch);
        return delegateMap.get(currentBranch);
    }

    @Override
    public boolean processNotify(MagicNotify notify) {
        // 收到集群通知，直接更新代码并清空缓存 todo 优化性能
        synchronized (gitRepo) {
            gitRepo.update(true);
            resetDelegateMap(true);
            return true;
        }
    }

    @Override
    public void refresh() {
        getDelegate().refresh();
    }

    @Override
    public Resource getResource() {
        return getDelegate().getResource();
    }

    @Override
    public boolean saveGroup(Group group) {
        return getDelegate().saveGroup(group);
    }

    @Override
    public boolean move(String src, String groupId) {
        return getDelegate().move(src, groupId);
    }

    @Override
    public String copyGroup(String src, String groupId) {
        return getDelegate().copyGroup(src, groupId);
    }

    @Override
    public TreeNode<Group> tree(String type) {
        return getDelegate().tree(type);
    }

    @Override
    public Map<String, TreeNode<Group>> tree() {
        return getDelegate().tree();
    }

    @Override
    public List<Group> getGroupsByFileId(String id) {
        return getDelegate().getGroupsByFileId(id);
    }

    @Override
    public Resource getGroupResource(String id) {
        return getDelegate().getGroupResource(id);
    }

    @Override
    public <T extends MagicEntity> boolean saveFile(T entity) {
        return getDelegate().saveFile(entity);
    }

    @Override
    public boolean delete(String id) {
        return getDelegate().delete(id);
    }

    @Override
    public <T extends MagicEntity> List<T> listFiles(String groupId) {
        return getDelegate().listFiles(groupId);
    }

    @Override
    public <T extends MagicEntity> List<T> files(String type) {
        return getDelegate().files(type);
    }

    @Override
    public <T extends MagicEntity> T file(String id) {
        return getDelegate().file(id);
    }

    @Override
    public Group getGroup(String id) {
        return getDelegate().getGroup(id);
    }

    @Override
    public void export(String groupId, List<SelectedResource> resources, OutputStream os) throws IOException {
        getDelegate().export(groupId, resources, os);
        throw new UnsupportedOperationException("can not export");
    }

    @Override
    public boolean lock(String id) {
        return getDelegate().lock(id);
    }

    @Override
    public boolean unlock(String id) {
        return getDelegate().unlock(id);
    }

    @Override
    public boolean upload(InputStream inputStream, boolean full) throws IOException {
        return getDelegate().upload(inputStream, full);
    }

    @Override
    public String getGroupName(String groupId) {
        return getDelegate().getGroupName(groupId);
    }

    @Override
    public String getGroupPath(String groupId) {
        return getDelegate().getGroupPath(groupId);
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        resetDelegateMap(false);
    }
}
