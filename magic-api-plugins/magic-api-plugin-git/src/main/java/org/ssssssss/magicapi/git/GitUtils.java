package org.ssssssss.magicapi.git;

import org.apache.commons.lang3.StringUtils;
import org.ssssssss.magicapi.core.servlet.MagicHttpServletRequest;
import org.ssssssss.magicapi.utils.WebUtils;

/**
 * @author zengnianmei
 */
public class GitUtils {
    private final static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void setCurrentBranch(String branch) {
        threadLocal.set(branch);
    }

    public static String getCurrentBranch(String branchHeader, String baseBranch) {
        String branch = threadLocal.get();
        if (StringUtils.isNotBlank(branch)) {
            return branch;
        }

        if (StringUtils.isBlank(branchHeader)) {
            return baseBranch;
        }
        MagicHttpServletRequest request = WebUtils.magicRequestContextHolder.getRequest();
        if (request == null) {
            return baseBranch;
        }
        branch = request.getHeader(branchHeader);
        if (StringUtils.isBlank(branch)) {
            return baseBranch;
        }
        return branch;
    }
}
