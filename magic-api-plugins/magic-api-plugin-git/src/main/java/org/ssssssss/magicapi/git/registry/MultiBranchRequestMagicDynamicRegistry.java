package org.ssssssss.magicapi.git.registry;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.ssssssss.magicapi.core.config.MagicConfiguration;
import org.ssssssss.magicapi.core.event.FileEvent;
import org.ssssssss.magicapi.core.event.GroupEvent;
import org.ssssssss.magicapi.core.event.MagicEvent;
import org.ssssssss.magicapi.core.exception.InvalidArgumentException;
import org.ssssssss.magicapi.core.exception.MagicAPIException;
import org.ssssssss.magicapi.core.model.ApiInfo;
import org.ssssssss.magicapi.core.service.MagicResourceStorage;
import org.ssssssss.magicapi.core.service.impl.RequestMagicDynamicRegistry;
import org.ssssssss.magicapi.core.servlet.MagicHttpServletRequest;
import org.ssssssss.magicapi.core.servlet.MagicHttpServletResponse;
import org.ssssssss.magicapi.core.web.RequestHandler;
import org.ssssssss.magicapi.git.GitUtils;
import org.ssssssss.magicapi.git.MagicGitProperties;
import org.ssssssss.magicapi.utils.Mapping;
import org.ssssssss.magicapi.utils.PathUtils;
import org.ssssssss.magicapi.utils.ScriptManager;
import org.ssssssss.script.MagicResourceLoader;
import org.ssssssss.script.MagicScriptContext;
import org.ssssssss.script.exception.MagicExitException;
import org.ssssssss.script.runtime.ExitValue;
import org.ssssssss.script.runtime.function.MagicScriptLambdaFunction;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.ssssssss.magicapi.core.config.JsonCodeConstants.REQUEST_PATH_CONFLICT;

/**
 * RequestMagicDynamicRegistry 的多分支支持
 *
 * @author zengnianmei
 */
public class MultiBranchRequestMagicDynamicRegistry extends RequestMagicDynamicRegistry {
    private final static Logger logger = LoggerFactory.getLogger(MultiBranchRequestMagicDynamicRegistry.class);
    private final MagicResourceStorage<ApiInfo> magicResourceStorage;
    private final Mapping mapping;
    private final boolean allowOverride;
    private final MagicGitProperties gitProperties;
    private final String prefix;
    private Object handler;
    private final Method method = RequestHandler.class.getDeclaredMethod("invoke", MagicHttpServletRequest.class,
            MagicHttpServletResponse.class, Map.class, Map.class, Map.class);

    private final Map<String, RequestMagicDynamicRegistry> delegateMap = new HashMap<>();

    public MultiBranchRequestMagicDynamicRegistry(MagicResourceStorage<ApiInfo> magicResourceStorage, Mapping mapping,
                                                  boolean allowOverride, String prefix,
                                                  MagicGitProperties gitProperties) throws NoSuchMethodException {
        // 这里的super构造器将执行addFunctionLoader，但内容为空
        super(magicResourceStorage, mapping, allowOverride, prefix);
        this.magicResourceStorage = magicResourceStorage;
        this.mapping = mapping;
        this.allowOverride = allowOverride;
        this.prefix = StringUtils.defaultIfBlank(prefix, "")+"/";
        this.gitProperties = gitProperties;
        MagicResourceLoader.addFunctionLoader(this::lookupLambdaFunction);
    }

    private Object lookupLambdaFunction(MagicScriptContext context, String name) {
        int index = name.indexOf(":");
        if (index > -1) {
            RequestMagicDynamicRegistry registry = getCurrentBranchRegistry(context);
            if (registry == null) {
                return null;
            }
            String method = name.substring(0, index);
            String path = name.substring(index+1);
            String mappingKey = method.toUpperCase()+":"+PathUtils.replaceSlash(this.prefix+path);
            ApiInfo info = registry.getMapping(mappingKey);
            if (info != null) {
                String scriptName = MagicConfiguration.getMagicResourceService().getScriptName(info);
                return (MagicScriptLambdaFunction) (variables, args) -> {
                    MagicScriptContext newContext = new MagicScriptContext();
                    Map<String, Object> varMap = new LinkedHashMap<>(context.getRootVariables());
                    varMap.putAll(variables.getVariables(context));
                    newContext.setScriptName(scriptName);
                    newContext.putMapIntoContext(varMap);
                    Object value = ScriptManager.executeScript(info.getScript(), newContext);
                    if (value instanceof ExitValue) {
                        throw new MagicExitException((ExitValue) value);
                    }
                    return value;
                };
            }
        }
        return null;
    }

    private RequestMagicDynamicRegistry getCurrentBranchRegistry(MagicScriptContext context) {
        Map header = MapUtils.getMap(context.getRootVariables(), "header");
        String branch = MapUtils.getString(header, gitProperties.getBranchHeader());
        if (StringUtils.isBlank(branch)) {
            branch = gitProperties.getBranch();
        }
        return delegateMap.get(branch);
    }

    @Override
    public ApiInfo getApiInfoFromRequest(MagicHttpServletRequest request) {
        String branchHeader = gitProperties.getBranchHeader();
        String branch = request.getHeader(branchHeader);
        if (StringUtils.isBlank(branch)) {
            branch = gitProperties.getBranch();
        }
        RequestMagicDynamicRegistry dynamicRegistry = delegateMap.get(branch);
        if (dynamicRegistry != null) {
            return dynamicRegistry.getApiInfoFromRequest(request);
        }
        return null;
    }

    public void setHandler(Object handler) {
        this.handler = handler;
        delegateMap.forEach((k, delegate) -> delegate.setHandler(handler));
    }

    private RequestMagicDynamicRegistry getCurrentBranchRegistry() {
        String branchHeader = gitProperties.getBranchHeader();
        String baseBranch = gitProperties.getBranch();
        String currentBranch = GitUtils.getCurrentBranch(branchHeader, baseBranch);
        return delegateMap.compute(currentBranch, (key, old) -> {
            if (old != null) {
                return old;
            }
            try {
                RequestMagicDynamicRegistry dynamicRegistry = new RequestMagicDynamicRegistry(magicResourceStorage,
                        mapping, allowOverride, prefix) {
                    Map<RequestMappingInfo, RequestMappingInfo> requestBaseMappings = new HashMap<>();

                    @Override
                    public boolean register(MappingNode<ApiInfo> mappingNode) {
                        String mappingKey = mappingNode.getMappingKey();
                        int index = mappingKey.indexOf(":");
                        String requestMethod = mappingKey.substring(0, index);
                        String path = mappingKey.substring(index+1);
                        RequestMappingInfo.Builder builder = mapping.paths(path)
                                .methods(RequestMethod.valueOf(requestMethod.toUpperCase()));
                        String header = String.format("%s=%s", branchHeader, currentBranch);
                        RequestMappingInfo requestMappingInfo = builder.headers(header).build();
                        if (mapping.getHandlerMethods().containsKey(requestMappingInfo)) {
                            if (!allowOverride) {
                                logger.error("接口[{}({})]与应用冲突，无法注册", mappingNode.getEntity().getName(), mappingKey);
                                throw new InvalidArgumentException(REQUEST_PATH_CONFLICT.format(mappingNode.getEntity().getName(), mappingKey));
                            }
                            logger.warn("取消注册应用接口:{}", requestMappingInfo);
                            // 取消注册原接口
                            mapping.unregister(requestMappingInfo);
                        }
                        logger.debug("注册接口[{}({})]", mappingNode.getEntity().getName(), mappingKey);
                        mapping.register(requestMappingInfo, handler, method);
                        if (currentBranch.equals(baseBranch)) {
                            // 如果是基线环境的分支，还得注册不限header的请求映射
                            header = String.format("!%s", branchHeader);
                            RequestMappingInfo baseRequestMappingInfo = builder.headers(header).build();
                            mapping.register(baseRequestMappingInfo, handler, method);
                            requestBaseMappings.put(requestMappingInfo, baseRequestMappingInfo);

                        }
                        mappingNode.setMappingData(requestMappingInfo);
                        return true;
                    }

                    @Override
                    protected void unregister(MappingNode<ApiInfo> mappingNode) {
                        super.unregister(mappingNode);
                        RequestMappingInfo baseMapping = requestBaseMappings.remove(mappingNode.getMappingData());
                        if (baseMapping != null) {
                            mapping.unregister(baseMapping);
                        }
                    }
                };
                dynamicRegistry.setHandler(handler);
                return dynamicRegistry;
            } catch (NoSuchMethodException e) {
                throw new MagicAPIException("初始化RequestMagicDynamicRegistry失败", e);
            }
        });
    }

    @EventListener(condition = "#event.action == T(org.ssssssss.magicapi.core.event.EventAction).CLEAR")
    public void clear(MagicEvent event) {
        getCurrentBranchRegistry().clear(event);
    }

    @EventListener(condition = "#event.type == 'api'")
    public void onFileEvent(FileEvent event) {
        getCurrentBranchRegistry().onFileEvent(event);
    }

    @EventListener(condition = "#event.type == 'api'")
    public void onGroupEvent(GroupEvent event) {
        getCurrentBranchRegistry().onGroupEvent(event);
    }
}
