package org.ssssssss.magicapi.git.registry;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.event.EventListener;
import org.ssssssss.magicapi.core.config.MagicConfiguration;
import org.ssssssss.magicapi.core.event.FileEvent;
import org.ssssssss.magicapi.core.event.GroupEvent;
import org.ssssssss.magicapi.core.event.MagicEvent;
import org.ssssssss.magicapi.core.model.Parameter;
import org.ssssssss.magicapi.core.service.MagicResourceStorage;
import org.ssssssss.magicapi.function.model.FunctionInfo;
import org.ssssssss.magicapi.function.service.FunctionMagicDynamicRegistry;
import org.ssssssss.magicapi.git.GitUtils;
import org.ssssssss.magicapi.git.MagicGitProperties;
import org.ssssssss.magicapi.utils.ScriptManager;
import org.ssssssss.script.MagicResourceLoader;
import org.ssssssss.script.MagicScriptContext;
import org.ssssssss.script.exception.MagicExitException;
import org.ssssssss.script.runtime.ExitValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * FunctionMagicDynamicRegistry 的多分支支持
 *
 * @author zengnianmei
 */
public class MultiBranchFunctionMagicDynamicRegistry extends FunctionMagicDynamicRegistry {

    private final Map<String, FunctionMagicDynamicRegistry> delegateMap = new HashMap<>();
    private final MagicGitProperties gitProperties;

    public MultiBranchFunctionMagicDynamicRegistry(MagicResourceStorage<FunctionInfo> magicResourceStorage,
                                                   MagicGitProperties gitProperties) {
        super(magicResourceStorage);
        this.gitProperties = gitProperties;
        MagicResourceLoader.addFunctionLoader(this::lookupLambdaFunction);
    }


    private Object lookupLambdaFunction(MagicScriptContext context, String path) {
        FunctionMagicDynamicRegistry registry = getCurrentBranchRegistry(context);
        if (registry == null) {
            return null;
        }
        FunctionInfo functionInfo = registry.getMapping(path);
        if (functionInfo != null) {
            String scriptName = MagicConfiguration.getMagicResourceService().getScriptName(functionInfo);
            List<Parameter> parameters = functionInfo.getParameters();
            return (Function<Object[], Object>) objects -> {
                MagicScriptContext functionContext = new MagicScriptContext(context.getRootVariables());
                functionContext.setScriptName(scriptName);
                if (objects != null) {
                    for (int i = 0, len = objects.length, size = parameters.size(); i < len && i < size; i++) {
                        functionContext.set(parameters.get(i).getName(), objects[i]);
                    }
                }
                Object value = ScriptManager.executeScript(functionInfo.getScript(), functionContext);
                if (value instanceof ExitValue) {
                    throw new MagicExitException((ExitValue) value);
                }
                return value;
            };
        }
        return null;
    }

    private FunctionMagicDynamicRegistry getCurrentBranchRegistry(MagicScriptContext context) {
        Map header = MapUtils.getMap(context.getRootVariables(), "header");
        String branch = MapUtils.getString(header, gitProperties.getBranchHeader());
        if (StringUtils.isBlank(branch)) {
            branch = gitProperties.getBranch();
        }
        return delegateMap.get(branch);
    }

    private FunctionMagicDynamicRegistry getCurrentBranchRegistry() {
        String branchHeader = gitProperties.getBranchHeader();
        String baseBranch = gitProperties.getBranch();
        String currentBranch = GitUtils.getCurrentBranch(branchHeader, baseBranch);
        return delegateMap.compute(currentBranch, (key, old) -> {
            if (old == null) {
                return new FunctionMagicDynamicRegistry(magicResourceStorage);
            }
            return old;
        });
    }

    @EventListener(condition = "#event.action == T(org.ssssssss.magicapi.core.event.EventAction).CLEAR")
    public void clear(MagicEvent event) {
        getCurrentBranchRegistry().clear(event);
    }

    @EventListener(condition = "#event.type == 'function'")
    public void onFileEvent(FileEvent event) {
        getCurrentBranchRegistry().onFileEvent(event);
    }

    @EventListener(condition = "#event.type == 'function'")
    public void onGroupEvent(GroupEvent event) {
        getCurrentBranchRegistry().onGroupEvent(event);
    }
}
